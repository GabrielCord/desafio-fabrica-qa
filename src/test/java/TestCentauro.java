import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestCentauro {
    private WebDriver driver;
    @Before
    // 1- Entre em um grande portal de comércio online
    public void open() {
        // Local onde o Selenium abre o drive do firefox
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\gabri\\Documents\\fabrica\\geckodriver-v0.31.0-win64\\geckodriver.exe");
        driver = new FirefoxDriver();
        // Abrir uma janela do drive
        driver.manage().window().maximize();
        // Insirir no navegador a instrução para ir no site da Amazon
        driver.get("https://www.centauro.com.br/");
        // Checar se abriu corretamente o site da Amazon atraves do title
        Assert.assertEquals("Centauro Loja de Esportes - Nike, Adidas, Mizuno, Asics, Oakley e mais! - Seu esporte, nossa paixão", driver.getTitle());
    }

    @After
    public void close() {
        driver.quit();
    }

    @Test
    public void searchProductInWebsite()throws InterruptedException{
        // 2- Faça uma busca por um produto
        // Escrever Iphone 13 pro max no buscador e clicar para pesquisar
        driver.findElement(By.xpath("//*[@id=\"searchInput\"]")).sendKeys("mochila");
        driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/header/div[2]/div[2]/div[1]/div[2]/div/form/div")).click();
        Thread.sleep(1000);

        // 3- Valide o retorno da busca
        Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div[1]/h1")).getText().contains("mochila"));
        Thread.sleep(1000);

        // 4- Escolha um produto na lista
        driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div[1]/div[3]/div[2]/div[2]/div[1]")).click();
        Thread.sleep(1000);

        // 5- Adicione o carrinho
        driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div[1]/section[4]/div/div[5]/div[1]/div[1]/div")).click();
        Thread.sleep(1000);

        // 6- Valide o produto no carrinho
        Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div[1]/div/div[2]/div/div[1]/div/div[1]/div/div[1]/div[1]/span")).getText().contains("MEU CARRINHO"));

    }


}